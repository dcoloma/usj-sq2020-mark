function getMark(marks) { // eslint-disable-line no-unused-vars
  var finalMark = 0;

  var weights = {
    test1: 10, test2: 10,
    task1: 12.5, task2: 12.5, task3: 12.5, task4: 12.5, task5: 20,
    participation: 10
  };

  var keys = Object.keys(marks);

  keys.forEach(function (name) { 
    var mark = marks[name];
    if (isNaN(mark) || (mark<0) || (mark>100)) {
      return 'INVALID ARGUMENTS, marks must be a number between 0 and 100';
    } else {
      finalMark += weights[name]*mark;
    }
  });

  if (marks.test1 < 40 || marks.test2 < 40)
    finalMark = 0;

  return finalMark / 100;
}
