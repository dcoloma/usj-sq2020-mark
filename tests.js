test('Boundaries', function (assert) {
  assert.equal(getMark({ test1: 100, test2: 100, task1: 100, task2: 100, task3: 100, task4: 100, task5: 100, participation: 100 }), 100, "PERFECT SCORE");
  assert.equal(getMark({ test1: 0, test2: 0, task1: 0, task2: 0, task3: 0, task4: 0, task5: 0, participation: 0 }), 0, "PERFECT SCORE");
  assert.equal(getMark({ test1: 50, test2: 50, task1: 50, task2: 50, task3: 50, task4: 50, task5: 50, participation: 49 }), 49.9, "JUST BELOW 50");
  assert.equal(getMark({ test1: 50, test2: 50, task1: 50, task2: 50, task3: 50, task4: 50, task5: 50, participation: 50 }), 50, "PERFECT AVERAGE");
});

test('Average', function (assert) {
  assert.equal(getMark({ test1: 40, test2: 40, task1: 40, task2: 40, task3: 40, task4: 40, task5: 40, participation: 40 }), 40, "BELOW 5");
  assert.equal(getMark({ test1: 60, test2: 60, task1: 60, task2: 60, task3: 60, task4: 60, task5: 60, participation: 60 }), 60, "ABOVE 5");
});

test('Average', function (assert) {
  assert.equal(getMark({ test1: 100, test2: 39, task1: 40, task2: 39, task3: 56, task4: 49, task5: 44, participation: 43 }), 0, "JUST 5 BUT TEST UNDER 0");
  assert.equal(getMark({ test1: 39, test2: 100, task1: 40, task2: 40, task3: 40, task4: 60, task5: 60, participation: 16 }), 0, "JUST 5 BUT TEST UNDER 0");
  assert.equal(getMark({ test1: 100, test2: 39, task1: 100, task2: 100, task3: 100, task4: 100, task5: 100, participation: 100 }), 0, "JUST 5 BUT TEST UNDER 0");
});

